package com.raymond.currencyconverter.di

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.raymond.currencyconverter.BuildConfig
import com.raymond.currencyconverter.api.CurrencyConverterApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

val apiModule = module {
  single {
    Retrofit.Builder()
      .addConverterFactory(GsonConverterFactory.create(get()))
      .client(get())
      .baseUrl(BuildConfig.BASE_URL)
      .build()
  }
  single {
    OkHttpClient.Builder()
      .addNetworkInterceptor(get<HttpLoggingInterceptor>())
      .build()
  }
  factory {
    GsonBuilder()
      .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
      .create()
  }
  factory { HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY } }
  single { get<Retrofit>().create<CurrencyConverterApi>() }
}