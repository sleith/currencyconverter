package com.raymond.currencyconverter.di

import androidx.room.Room
import com.raymond.currencyconverter.data.CurrencySharedPrefs
import com.raymond.currencyconverter.data.repository.CurrencyRepository
import com.raymond.currencyconverter.data.room.CurrencyDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val storageModule = module {
  single { CurrencySharedPrefs(androidContext()) }
  single {
    Room.databaseBuilder(
      androidContext(),
      CurrencyDatabase::class.java,
      "currency_database"
    ).build()
  }
  factory { CurrencyRepository(get(), get()) }
}
