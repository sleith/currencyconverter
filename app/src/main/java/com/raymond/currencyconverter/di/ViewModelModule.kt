package com.raymond.currencyconverter.di

import com.raymond.currencyconverter.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import kotlin.time.DurationUnit
import kotlin.time.toDuration

val viewModelModule = module {
  viewModel {
    MainViewModel(30.toDuration(DurationUnit.MINUTES), get(), get())
  }
}