package com.raymond.currencyconverter

import android.app.Application
import com.raymond.currencyconverter.di.apiModule
import com.raymond.currencyconverter.di.storageModule
import com.raymond.currencyconverter.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MainApplication : Application() {
  override fun onCreate() {
    super.onCreate()

    initDependencyInjection()
  }

  private fun initDependencyInjection() {
    startKoin {
      androidContext(applicationContext)
      modules(
        listOf(
          apiModule,
          storageModule,
          viewModelModule,
        )
      )
    }
  }
}