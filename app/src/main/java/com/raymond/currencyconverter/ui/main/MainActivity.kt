package com.raymond.currencyconverter.ui.main

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.raymond.currencyconverter.databinding.ActivityMainBinding
import com.raymond.currencyconverter.ui.main.adapter.RateAdapter
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

  private lateinit var binding: ActivityMainBinding
  private val viewModel: MainViewModel by viewModel()

  private val spinnerCurrencyAdapter by lazy {
    ArrayAdapter<MainViewModel.CurrencyItem>(this, android.R.layout.simple_spinner_dropdown_item)
  }
  private val rateAdapter by lazy { RateAdapter() }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    binding = ActivityMainBinding.inflate(layoutInflater)
    val view = binding.root
    setContentView(view)

    initViews()
    initVmObserver()
  }

  private fun initViews() = with(binding) {
    spinnerCurrency.adapter = spinnerCurrencyAdapter
    spinnerCurrency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
      override fun onItemSelected(adapter: AdapterView<*>?, p1: View?, position: Int, id: Long) {
        val item = adapter?.getItemAtPosition(position) as MainViewModel.CurrencyItem
        viewModel.onTargetCurrencyChanged(item)
      }

      override fun onNothingSelected(p0: AdapterView<*>?) {

      }
    }
    gridCurrencies.adapter = rateAdapter
    editPrice.doOnTextChanged { text, _, _, _ -> viewModel.onInputPriceChanged(text?.toString()) }
  }

  private fun initVmObserver() {
    viewModel.state
      .flowWithLifecycle(lifecycle)
      .onEach { state ->
        state.currencies?.handle {
          spinnerCurrencyAdapter.clear()
          spinnerCurrencyAdapter.addAll(it)
        }
        rateAdapter.updateItems(state.rateItems)
      }
      .launchIn(lifecycleScope)
  }
}