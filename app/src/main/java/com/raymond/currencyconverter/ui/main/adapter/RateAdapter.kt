package com.raymond.currencyconverter.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.raymond.currencyconverter.databinding.ItemCurrencyRateBinding
import com.raymond.currencyconverter.ui.main.MainViewModel

class RateAdapter : BaseAdapter() {
  private var items: List<MainViewModel.RateItem> = emptyList()

  fun updateItems(newItems: List<MainViewModel.RateItem>) {
    items = newItems
    notifyDataSetChanged()
  }

  override fun getCount(): Int = items.size

  override fun getItem(position: Int): MainViewModel.RateItem = items[position]

  override fun getItemId(position: Int): Long = position.toLong()

  override fun getView(
    position: Int,
    convertView: View?,
    parent: ViewGroup
  ): View {
    val viewHolder: ViewHolder = if (convertView == null) {
      val binding = ItemCurrencyRateBinding.inflate(LayoutInflater.from(parent.context))
      val newViewHolder = ViewHolder(binding)
      newViewHolder.getView().tag = newViewHolder
      newViewHolder
    } else {
      convertView.tag as ViewHolder
    }
    viewHolder.bindItem(getItem(position))
    return viewHolder.getView()
  }

  private class ViewHolder(private val binding: ItemCurrencyRateBinding) {
    fun bindItem(item: MainViewModel.RateItem) {
      binding.textCurrency.text = item.currency
      binding.textRate.text = item.rate
    }

    fun getView() = binding.root
  }
}