package com.raymond.currencyconverter.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.raymond.currencyconverter.common.viewmodel.Event
import com.raymond.currencyconverter.data.CurrencySharedPrefs
import com.raymond.currencyconverter.data.model.Rates
import com.raymond.currencyconverter.data.repository.CurrencyRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import java.util.Timer
import java.util.TimerTask
import kotlin.time.Duration
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class MainViewModel(
  private val refreshDuration: Duration,
  private val sharedPrefs: CurrencySharedPrefs,
  private val repository: CurrencyRepository,
) : ViewModel() {
  private val _state = MutableStateFlow(State())
  val state: StateFlow<State> by lazy {
    onViewStartObserving()
    _state
  }
  private var currencies: List<CurrencyItem> = emptyList()
  private var rates: Rates? = null
  private var targetPrice: Double = 1.0
  private var targetCurrency: String = ""
  private var timer = Timer()

  private fun onViewStartObserving() {
    loadFromStorage()
    setNextTimerToFetchFromApi()
  }

  override fun onCleared() {
    timer.cancel()
    super.onCleared()
  }

  private fun loadFromStorage() {
    viewModelScope.launch {
      repository.getRates(fetchFromApi = false).let(::updateRate)
      repository.getCurrencies(fetchFromApi = false).let(::updateCurrencyItems)
    }
  }

  private fun setNextTimerToFetchFromApi() {
    val diffLastFetchMillis =
      System.currentTimeMillis() - sharedPrefs.getLastFetchFromApiMillis()
    val millisToWait = (refreshDuration.inWholeMilliseconds - diffLastFetchMillis)
      .coerceAtLeast(0)

    timer.schedule(object : TimerTask() {
      override fun run() {
        viewModelScope.launch(Dispatchers.IO) {
          try {
            repository.getCurrencies(fetchFromApi = true).let {
              repository.insertCurrencies(it)
              updateCurrencyItems(it)
            }
            repository.getRates(fetchFromApi = true).let {
              repository.insertRates(it)
              updateRate(it)
            }
            sharedPrefs.setFetchedFromApi()
          } catch (e: Exception) {
            e.printStackTrace()
            delay(3.toDuration(DurationUnit.SECONDS))
          }
          setNextTimerToFetchFromApi()
        }
      }
    }, millisToWait)
  }

  private fun updateRate(rate: Rates) {
    rates = rate
    updateRateItems()
  }

  private fun updateCurrencyItems(currencies: Map<String, String>) {
    this.currencies = currencies.map {
      CurrencyItem(currency = it.key, description = it.value)
    }
    _state.value = _state.value.copy(currencies = Event(this.currencies))
  }

  fun onTargetCurrencyChanged(item: CurrencyItem) {
    targetCurrency = item.currency
    updateRateItems()
  }

  fun onInputPriceChanged(text: String?) {
    targetPrice = text?.toDoubleOrNull()?.coerceAtLeast(0.0) ?: 1.0
    updateRateItems()
  }

  private fun updateRateItems() {
    val targetCurrencyRate = rates?.rates?.get(targetCurrency) ?: 1.0
    val rateToConvert = 1.0f / targetCurrencyRate
    val items = rates?.let { rate ->
      rate.rates.map {
        val calculatedPrice = it.value * rateToConvert * targetPrice

        val format = DecimalFormat("#,###.##")
        RateItem(it.key, format.format(calculatedPrice))
      }
    } ?: emptyList()

    _state.value = _state.value.copy(rateItems = items)
  }

  data class CurrencyItem(val currency: String, val description: String) {
    override fun toString(): String {
      return "$currency - $description"
    }
  }

  data class RateItem(val currency: String, val rate: String)
  data class State(
    val currencies: Event<List<CurrencyItem>>? = null,
    val rateItems: List<RateItem> = emptyList(),
  )
}