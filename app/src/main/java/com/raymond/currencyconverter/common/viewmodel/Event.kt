package com.raymond.currencyconverter.common.viewmodel

import androidx.annotation.MainThread

class Event<out T : Any>(private val content: T) {
  private var isHandled = false

  @MainThread
  private fun getContentIfNotHandled(): T? {
    return if (isHandled) {
      null
    } else {
      isHandled = true
      content
    }
  }

  @MainThread
  fun handle(block: (T) -> Unit) {
    getContentIfNotHandled()?.let(block)
  }
}