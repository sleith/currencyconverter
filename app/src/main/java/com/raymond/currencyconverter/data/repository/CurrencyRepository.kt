package com.raymond.currencyconverter.data.repository

import com.raymond.currencyconverter.api.CurrencyConverterApi
import com.raymond.currencyconverter.data.model.Rates
import com.raymond.currencyconverter.data.room.CurrencyDatabase
import com.raymond.currencyconverter.data.room.entities.CurrencyEntity
import com.raymond.currencyconverter.data.room.entities.RateEntity

class CurrencyRepository(
  private val currencyDatabase: CurrencyDatabase,
  private val api: CurrencyConverterApi,
) {
  suspend fun insertCurrencies(data: Map<String, String>) {
    val items = data.map { CurrencyEntity(name = it.key, description = it.value) }
    currencyDatabase.currencyDao().insertAll(items)
  }

  suspend fun insertRates(data: Rates) {
    val items = data.rates.map { RateEntity(currency = it.key, rate = it.value) }
    currencyDatabase.rateDao().insertAll(items)
  }

  suspend fun getCurrencies(fetchFromApi: Boolean): Map<String, String> {
    return if (fetchFromApi) {
      api.getCurrencies()
    } else {
      val map: HashMap<String, String> = hashMapOf()
      currencyDatabase.currencyDao().getAll().forEach { map[it.name] = it.description }
      map.toSortedMap()
    }
  }

  suspend fun getRates(fetchFromApi: Boolean): Rates {
    return if (fetchFromApi) {
      api.getAllRates()
    } else {
      val map: HashMap<String, Double> = hashMapOf()
      currencyDatabase.rateDao().getAll().forEach { map[it.currency] = it.rate }
      Rates(map.toSortedMap())
    }
  }
}