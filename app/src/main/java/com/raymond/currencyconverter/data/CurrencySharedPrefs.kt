package com.raymond.currencyconverter.data

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

class CurrencySharedPrefs(applicationContext: Context) {
  private val sharedPreferences: SharedPreferences =
    applicationContext.getSharedPreferences("currency_pref", Context.MODE_PRIVATE)

  fun getLastFetchFromApiMillis(): Long {
    return sharedPreferences.getLong("last_fetch", 0)
  }

  fun setFetchedFromApi() {
    sharedPreferences.edit { putLong("last_fetch", System.currentTimeMillis()) }
  }
}