package com.raymond.currencyconverter.data.room.entities

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Keep
@Entity(tableName = "currency_table")
class CurrencyEntity(
  @PrimaryKey @ColumnInfo(name = "name") val name: String,
  @ColumnInfo(name = "description") val description: String,
)