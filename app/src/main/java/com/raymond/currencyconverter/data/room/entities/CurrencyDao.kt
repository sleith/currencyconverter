package com.raymond.currencyconverter.data.room.entities

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CurrencyDao {
  @Query("SELECT * FROM currency_table ORDER BY name ASC")
  suspend fun getAll(): List<CurrencyEntity>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun insertAll(items: List<CurrencyEntity>)
}