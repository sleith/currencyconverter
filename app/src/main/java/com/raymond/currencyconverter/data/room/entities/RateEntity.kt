package com.raymond.currencyconverter.data.room.entities

import androidx.annotation.Keep
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Keep
@Entity(tableName = "rate_table")
class RateEntity(
  @PrimaryKey @ColumnInfo(name = "currency") val currency: String,
  @ColumnInfo(name = "rate") val rate: Double,
)