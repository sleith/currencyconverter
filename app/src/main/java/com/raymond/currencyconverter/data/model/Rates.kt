package com.raymond.currencyconverter.data.model

import androidx.annotation.Keep

@Keep
class Rates(val rates: Map<String, Double>)