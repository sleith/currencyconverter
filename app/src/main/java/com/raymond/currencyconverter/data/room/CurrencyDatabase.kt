package com.raymond.currencyconverter.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.raymond.currencyconverter.data.room.entities.CurrencyDao
import com.raymond.currencyconverter.data.room.entities.CurrencyEntity
import com.raymond.currencyconverter.data.room.entities.RateDao
import com.raymond.currencyconverter.data.room.entities.RateEntity

@Database(
  entities = [CurrencyEntity::class, RateEntity::class],
  version = 1,
  exportSchema = false
)
abstract class CurrencyDatabase : RoomDatabase() {
  abstract fun currencyDao(): CurrencyDao
  abstract fun rateDao(): RateDao
}