package com.raymond.currencyconverter.data.room.entities

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface RateDao {
  @Query("SELECT * FROM rate_table ORDER BY currency ASC")
  suspend fun getAll(): List<RateEntity>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  suspend fun insertAll(items: List<RateEntity>)
}