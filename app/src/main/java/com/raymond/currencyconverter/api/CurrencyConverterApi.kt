package com.raymond.currencyconverter.api

import com.raymond.currencyconverter.BuildConfig
import com.raymond.currencyconverter.data.model.Rates
import retrofit2.http.GET

interface CurrencyConverterApi {
  @GET("currencies.json?app_id=${BuildConfig.API_KEY}")
  suspend fun getCurrencies(): Map<String, String>

  @GET("latest.json?app_id=${BuildConfig.API_KEY}")
  suspend fun getAllRates(): Rates
}