package com.raymond.currencyconverter.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.raymond.currencyconverter.data.CurrencySharedPrefs
import com.raymond.currencyconverter.data.model.Rates
import com.raymond.currencyconverter.data.repository.CurrencyRepository
import com.raymond.currencyconverter.rules.CoroutinesTestRule
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Rule
import org.junit.Test
import java.text.DecimalFormat
import kotlin.test.assertEquals
import kotlin.time.DurationUnit
import kotlin.time.toDuration

class MainViewModelTest {
  @get:Rule
  val instantTaskExecutorRule = InstantTaskExecutorRule()

  @get:Rule
  val coroutinesTestRule = CoroutinesTestRule()

  private val currencies = mapOf(
    "JPY" to "Japan Yen",
    "USD" to "United State Dollar",
  )
  private val rates = Rates(
    mapOf(
      "JPY" to 100.0,
      "USD" to 1.0,
    )
  )
  private val sharedPrefs: CurrencySharedPrefs = mockk(relaxed = true)
  private val repository: CurrencyRepository = mockk(relaxed = true) {
    coEvery { getCurrencies(any()) } returns currencies
    coEvery { getRates(any()) } returns rates
  }
  private val formatter = DecimalFormat("#,###.##")
  private val viewModel = MainViewModel(
    refreshDuration = 30.toDuration(DurationUnit.SECONDS),
    sharedPrefs = sharedPrefs,
    repository = repository,
  )

  @OptIn(ExperimentalCoroutinesApi::class)
  @Test
  fun onStartObserving_dataLoadedFromDatabaseImmediately() = runTest {
    viewModel.state
    coVerify { repository.getRates(fetchFromApi = false) }
    coVerify { repository.getCurrencies(fetchFromApi = false) }

    val currencyEvent = requireNotNull(viewModel.state.value.currencies)
    currencyEvent.handle {
      assertEquals(2, it.size)
      assertEquals("JPY", it[0].currency)
      assertEquals("USD", it[1].currency)
    }

    val rateItems = viewModel.state.value.rateItems
    assertEquals(2, rateItems.size)
    assertEquals(formatter.format(100.0), rateItems[0].rate)
    assertEquals(formatter.format(1.0), rateItems[1].rate)
  }

  @Test
  fun onInputPriceChanged_rateItemsUpdatedCorrectly() {
    viewModel.state
    viewModel.onInputPriceChanged("2")
    val rateItems = viewModel.state.value.rateItems
    assertEquals(2, rateItems.size)
    assertEquals(formatter.format(200.0), rateItems[0].rate)
    assertEquals(formatter.format(2.0), rateItems[1].rate)
  }

  @Test
  fun onTargetCurrencyChanged_rateItemsUpdatedCorrectly() {
    viewModel.state
    viewModel.onTargetCurrencyChanged(MainViewModel.CurrencyItem("JPY", ""))
    val rateItems = viewModel.state.value.rateItems
    assertEquals(2, rateItems.size)
    assertEquals(formatter.format(1.0), rateItems[0].rate)
    assertEquals(formatter.format(0.01), rateItems[1].rate)
  }
}